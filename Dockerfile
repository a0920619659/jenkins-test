FROM node:12.7-alpine AS build-step
WORKDIR /usr/src/app
COPY package.json package-lock.json ./
RUN npm install
COPY . .
RUN npm run build

### STAGE 2: Run ###
FROM nginx:1.17.1-alpine

COPY --from=build-step /usr/src/app/dist/jenkins-test /usr/share/nginx/html
COPY --from=build-step /usr/src/app/nginx.conf /etc/nginx/conf.d/default.conf

EXPOSE 80

# CMD ["npm", "run", "start"]